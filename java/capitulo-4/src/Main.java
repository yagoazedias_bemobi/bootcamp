import banco.Conta;
import banco.Funcionario;
import util.Data;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Funcionario> listaDeFuncionarios = new ArrayList<Funcionario>();
        listaDeFuncionarios.add(new Funcionario("Fulano", "finanças",2000, new Data(1, 2, 2015),"00.00.00.0-00"));

        System.out.print(listaDeFuncionarios.get(0).getNome());
    }
}

class Exercicio4 {
    public static void main(String[] args) {
        System.out.println(fatorial(60));
    }

    public static long fatorial(long n) {
        if(n == 1 || n == 0) {
            return 1;
        } else {
            return fatorial(n - 1) *  n;
        }
    }
}
class Exercicio5 {

    public static void main(String[] args) {
        math(13);
    }

    public static void math(int n) {
        while (n != 1) {

            if (n % 2 == 0) {
                n = n / 2;
            } else if (n % 2 != 0) {
                n *= 3;
                n += 1;
            }

            System.out.print(n + " -> ");
        }
    }
}
package br.com.bootcamp.Poligonos;

import br.com.bootcamp.Interfaces.IAreaCalculavel;

/**
 * Created by yagoazedias on 7/16/17.
 */
public class Quadrado implements IAreaCalculavel {

    private int lado;

    public Quadrado(int lado) {
        this.lado = lado;
    }

    @Override
    public double calculaArea() {
        return 0;
    }
}

package br.com.bootcamp.Interfaces;

/**
 * Created by yagoazedias on 7/16/17.
 */
public interface IAreaCalculavel {
    double calculaArea();
}

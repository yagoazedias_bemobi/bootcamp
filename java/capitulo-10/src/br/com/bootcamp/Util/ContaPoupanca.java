package br.com.bootcamp.Util;

/**
 * Created by yagoazedias on 7/16/17.
 */
public class ContaPoupanca extends Conta {

    private double saldo;

    @Override
    public void atualiza(double taxa) {
        this.saldo += this.saldo * taxa * 3;
    }

}

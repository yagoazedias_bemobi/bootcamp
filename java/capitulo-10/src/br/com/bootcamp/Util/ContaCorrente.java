package br.com.bootcamp.Util;

public class ContaCorrente extends Conta {

    private double saldo;

    @Override
    public void atualiza(double taxa) {
        this.saldo += this.saldo * taxa * 2;
    }

    @Override
    public void deposita(double valor) {
        this.saldo += valor - 0.10;
    }

}

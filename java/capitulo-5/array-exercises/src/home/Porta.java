package home;

/**
 * Created by yago.azedias on 7/5/17.
 */
public class Porta {

    private String material;
    private boolean estaAberta = false;

    public Porta(String material) {
        this.material = material;
    }

    public boolean estaAberta() {
        return estaAberta;
    }

    public void setOpen(boolean open) {
        estaAberta = open;
    }

    public String getMaterial() {
        return material;
    }
}

package home;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by yago.azedias on 7/5/17.
 */
public class Casa {

    private Color cor;
    private int totalDePortas;
    private Porta[] portas;

    public Casa(Color cor, Porta[] portas) {
        this.cor = cor;
        this.portas = portas;

        for (int i = 0; i < portas.length; i++) {
            this.portas[i] = new Porta("Madeira");
        }
    }

    public void Pinta(Color cor) {
        this.cor = cor;
    }

    public int quantasPortasEstaoAbertas() {
        List<Porta> portas = new ArrayList<Porta>();

        for (Porta p : this.portas) {
            if(p.estaAberta()) {
                portas.add(p);
            }
        }

        return portas.size();
    }

    public Color getCor() {
        return cor;
    }

    public int getTotalDePortas() {
        this.totalDePortas = portas.length;
        return totalDePortas;
    }

    public Porta[] getPortas() {
        return portas;
    }

    public void adcionaPorta(Porta porta) {
        Porta[] portas = new Porta[this.portas.length + 1];


        for(int i = 0; i < this.portas.length; i++) {
            portas[i] = this.portas[i];
        }

        portas[portas.length - 1] = porta;

        this.portas = portas;
    }
}
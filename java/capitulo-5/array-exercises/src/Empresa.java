import util.Funcionario;

/**
 * Created by yago.azedias on 7/4/17.
 */
public class Empresa {

    Funcionario[] empregados;

    String cnpj;
    public Empresa(Funcionario[] empregados, String cnpj) {
        this.empregados = empregados;
        this.cnpj = cnpj;
    }

    public Funcionario[] getEmpregados() {
        return empregados;
    }

    public void setEmpregados(Funcionario[] empregados) {
        this.empregados = empregados;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    void adiciona(Funcionario f) {

        for (int i = 0; i < empregados.length; i++) {
            if (empregados[i] == null) {
                empregados[i] = f;
                break;
            }
        }

    }
}

import home.Casa;
import home.Porta;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        Casa casa = new Casa(new Color(0,0,0), new Porta[10]);

        casa.adcionaPorta(new Porta("madeira"));
        casa.adcionaPorta(new Porta("madeira"));
        casa.adcionaPorta(new Porta("metal"));

        System.out.println(casa.getPortas()[10].getMaterial());
        System.out.println(casa.getPortas()[11].getMaterial());
        System.out.println(casa.getPortas()[12].getMaterial());
    }
}

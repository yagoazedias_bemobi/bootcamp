# Hard Compile

## Observação
Antes de utilizar de uma ide para cuidar da compliação dos arquivos '.java', é bom sofrer um pouco fazendo na mão, até para ter melhor compreeenssão de como é feito o processo.

## Passo a passo

- Escreva a classe java no arquiv **main.java**,  como mostrado abaixo:


``` java
class HelloWorld {
	public static void main(String[] args) {
		System.out.println("Hello World");
	}
}
```
- Compilar pelo terminar utilizando o compilador primário e assim criar o Bytecode da classe **HelloWorld.class** :

``` shell
javac main.java
```

- Fazer a JVM interpretar o Bytecode e então executar o programa:
``` shell
java HelloWorld
```

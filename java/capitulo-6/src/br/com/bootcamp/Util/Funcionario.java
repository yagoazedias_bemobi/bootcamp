package br.com.bootcamp.Util;

/**
 * Created by yago.azedias on 7/4/17.
 */
public class Funcionario {

    private String nome;
    private String departamento;
    private double salario;
    private Data dataDeEntrada;
    private String RG;

    public Funcionario() {

    }

    public Funcionario(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Data getDataDeEntrada() {
        return dataDeEntrada;
    }

    public void setDataDeEntrada(Data dataDeEntrada) {
        this.dataDeEntrada = dataDeEntrada;
    }

    public String getRG() {
        return RG;
    }

    public void setRG(String RG) {
        this.RG = RG;
    }

    public Conta getmConta() {
        return mConta;
    }

    public void setmConta(Conta mConta) {
        this.mConta = mConta;
    }

    public Conta mConta;

    public void recebeSalario() {
        this.mConta.deposita(salario);
    }

    void recebeAumento(double aumento) {
        this.salario += aumento;
    }

    double calculaGanhoAnual() {
        return this.salario * 12;
    }
}